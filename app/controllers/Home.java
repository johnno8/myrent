package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import models.*;

public class Home extends Controller
{
  /**
   * Renders Home (welcome) page
   */
  public static void welcome()
  {
    render();
  }
}
