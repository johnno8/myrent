package models;

import play.db.jpa.Model;
import java.util.*;
import javax.persistence.*;

@Entity
public class Administrator extends Model
{
  public String email;
  public String password;
  
  public Administrator()
  {
    this.email = "admin@witpress.ie";
    this.password = "secret";
  }
  
  /**
   * Checks if supplied email matches saved one
   * @param email
   * @return true if emails match, false otherwise
   */
  public boolean checkEmail(String email)
  {
    return this.email.equals(email);
  }
  
  /**
   * Checks if supplied password matches saved one
   * @param password
   * @return true if passwords match
   */
  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }
}
