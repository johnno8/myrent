 /**
  * Created by john on 16/08/2016.
  */
 // initialize the controls in the landlordConfig template
 $('.ui.dropdown.deleteRes').dropdown();
 $('.ui.dropdown.editRes').dropdown();

 //validate residence selection
 $('.ui.form.deleteRes').form({
   fields: {
     eircode: {
       identifier: 'eircode',
       rules: [{
         type: 'empty',
         prompt: 'Please select a residence',
       },],
     },
   },

 });

 // validate residence selection
 $('.ui.form.editRes').form({
   fields: {
     eircode: {
       identifier: 'eircode',
       rules: [{
         type: 'empty',
         prompt: 'Please select a residence',
       },],
     },
   },

 });