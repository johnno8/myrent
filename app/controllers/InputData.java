package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import models.*;
import org.json.simple.JSONObject;

public class InputData extends Controller
{
  /** Method to render InputData page. Checks if a user is logged in - if not
   * then login page is rendered; if yes, then InputData page is rendered with
   * the logged in user's details passed to the page
   */
  public static void index()
  {
	if(session.get("logged_in_landlordid") == null)
	{
	  Landlords.login();
	}
	else
	{
	  String userId = session.get("logged_in_landlordid");
	  Landlord landlord = Landlord.findById(Long.parseLong(userId));
	  render(landlord);
	}
  }
  
  /**
   * Adds a new residence object to the database
   * @param residence
   */
  public static void dataCapture(Residence residence)
  {
	Landlord landlord  = Landlords.getCurrentLandlord();
    residence.addLandlord(landlord);
    residence.registrationDate = new Date();
    landlord.addResidence(residence);
    residence.save();
    
    JSONObject obj = new JSONObject();
    String value = "Congratulations. You have successfully registered your " + residence.residenceType +".";
    obj.put("inputdata", value);
    renderJSON(obj);
  }
}
