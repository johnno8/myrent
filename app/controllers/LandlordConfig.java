package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import models.*;

public class LandlordConfig extends Controller
{
  /**
   * Renders a configuration page for the logged in landlord, passing the logged in Landlord,
   * and a list of all his residences to the page
   */
  public static void index()
  {
    Landlord landlord = Landlords.getCurrentLandlord();
    render(landlord);
  }
  
  /**
   * Deletes a Residence object based on its eircode, supplied as a parameter
   * @param eircode	the eircode of the residence to be deleted
   */
  public static void deleteResidence(String eircode)
  {
	Landlord landlord = Landlords.getCurrentLandlord();
	
    Residence residence = Residence.findByEircode(eircode);
    landlord.residences.remove(residence);
    
    landlord.save();
    residence.delete();
    index();
  }
  
  
}
