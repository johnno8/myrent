MyRent is a dynamic web app developed as an assignment as part of my programming and git summer school module. I began with a base app and incrementally developed a series of stories implementing improved functionality. The live app is viewable at [org-wit-myrent-svc-jokeeffe09.herokuapp.com](org-wit-myrent-svc-jokeeffe09.herokuapp.com).

## Valid Logins ##
### Administrator: ###
    User email: admin@witpress.ie
    Password: secret

### Landlord ###
    User email: homer@simpson.com
    Password: secret

### Tenant ###
    User email: barney@gumble.com
    Password: secret