package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
import utils.Circle;
import utils.Geodistance;
import utils.LatLng;

public class Report extends Controller 
{
  /**
  * This method executed before each action call in the controller.
  * Checks that a user has logged in.
  * If no user logged in the user is presented with the log in screen.
  */
  @Before
  static void checkAuthentification()
  {
    if(session.contains("logged_in_landlordid") == false)
    Landlords.login();
  }

  /**
  *  Generates a Report instance relating to all residences within circle
  * @param radius    The radius (metres) of the search area
  * @param latcenter The latitude of the centre of the search area
  * @param lngcenter The longtitude of the centre of the search area
  */
  public static void generateReport(double radius, double latcenter, double lngcenter)
  {
    // All reported residences will fall within this circle
    Circle circle = new Circle(latcenter, lngcenter, radius);
    Landlord landlord = Landlords.getCurrentLandlord();
    List<Residence> residences = new ArrayList<Residence>();
    // Fetch all residences and filter out those within circle
    List<Residence> residencesAll = Residence.findAll();
    for (Residence res : residencesAll)
    {
      LatLng residenceLocation = res.getGeolocation();
      if (Geodistance.inCircle(residenceLocation, circle))
      {
        residences.add(res);
      }
    }
    render("Report/renderReport.html", landlord, circle, residences);
  }

  public static void index()
  {
    render();
  }
  
}
