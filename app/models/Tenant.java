package models;

import javax.persistence.*;
import play.db.jpa.*;
import javax.persistence.*;

@Entity
public class Tenant extends Model
{
  public String firstName;
  
  public String lastName;
  
  public String email;
  
  public String password;
  
  /**
   * Having 'mappedBy="tenant"' here puts the foreign key in the Residence 
   * table, meaning that a residence may be deleted without having to delete the
   * associated tenant.
   */
  @OneToOne(mappedBy="tenant")
  public Residence residence;
  
  /**
   * Returns the tenant object associated with the supplied email
   * @param email
   * @return
   */
  public static Tenant findByEmail(String email)
  {
    return find("email", email).first();
  }
  
  /**
   * Checks if supplied password matches saved one
   * @param password
   * @return
   */
  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }
  
  /**
   * Returns the tenant's first name and last name as a string
   */
  public String toString()
  {
    return firstName + " " + lastName;
  }
}
