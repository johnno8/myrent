package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class EditProfile extends Controller
{
  /** Method to render EditProfile page. Checks if a user is logged in - if not
   * then login page is rendered; if yes, then EditProfile page is rendered with
   * the logged in user's details passed to the page
   */
  public static void index()
  {
    if(session.get("logged_in_landlordid") == null)
    {
      Landlords.login();
	}
    else
    {
      String userId = session.get("logged_in_landlordid");
      Landlord currentUser = Landlord.findById(Long.parseLong(userId));
      render(currentUser);
	}
  }
  
  /**
   * Method to allow a logged in user to edit some of their personal details
   * 
   * @param firstName
   * @param lastName
   * @param address1
   * @param address2
   * @param city
   * @param county
   */
  public static void changeProfile(String firstName, String lastName, String address1, String address2, String city, String county)
  {
	Landlord currentUser  = Landlords.getCurrentLandlord();
    
    currentUser.firstName = firstName;
    currentUser.lastName = lastName;
    currentUser.address1 = address1;
    currentUser.address2 = address2;
    currentUser.city = city;
    currentUser.county = county;
    
    currentUser.save();
    InputData.index();
  }
}
