package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class AdminReports extends Controller
{
  /**
   * Render index.html with a list of all residences
   */
  public static void index()
  {
    List<Residence> residences = Residence.findAll();
    render(residences);
  }
  
  /**
   * Render index.html with a list of all rented residences or vacant residences,
   * depending on the value of rentedStatus
   * @param rentedStatus	The rental status of the residence 
   */
  public static void byRented(String rentedStatus)
  {
	List<Residence> residencesAll = Residence.findAll();
    List<Residence> residences = new ArrayList();
    if(rentedStatus.equals("yes"))
    {
      for(Residence residence : residencesAll)
      {
        if(residence.tenant != null)
        {
          residences.add(residence);
        }
      }
    }
    if(rentedStatus.equals("no")) 
    {
      for(Residence residence : residencesAll)
      {
        if(residence.tenant == null)
        {
          residences.add(residence);
        }
      }
    }
    renderTemplate("AdminReports/index.html", residences);
  }
  
  /**
   * Render index.html with a list of all residences of a particular type,
   * may be flat, studio or house
   * @param residenceType	The type of building to filter by	
   */
  public static void byType(String residenceType)
  {
	List<Residence> residencesAll = Residence.findAll();
	List<Residence> residences = new ArrayList();
    if(residenceType.equals("flat"))
    {
      for(Residence residence : residencesAll)
      {
        if(residence.residenceType.equals("flat"))
        {
          residences.add(residence);
        }
      } 
    }
    if(residenceType.equals("studio"))
    {
      for(Residence residence : residencesAll)
      {
        if(residence.residenceType.equals("studio"))
        {
          residences.add(residence);
        }
      } 
    }
    if(residenceType.equals("house"))
    {
      for(Residence residence : residencesAll)
      {
        if(residence.residenceType.equals("house"))
        {
          residences.add(residence);
        }
      } 
    }
    
    renderTemplate("AdminReports/index.html", residences);
  }
  
  /**
   * Render index.html with a list of all residences sorted according to their
   * rent value
   * @param sortDirection	The direction of the sort (ascending or descending)
   */
  public static void bySortedRent(String sortDirection)
  {
    List<Residence> residences = Residence.findAll();
    
    if(sortDirection.equals("ascending"))
    {
      Collections.sort(residences, new Comparator<Residence>() {
        @Override
        public int compare(Residence r1, Residence r2) {
          return r1.rent - r2.rent;
        }
      });
    }
    
    if(sortDirection.equals("descending"))
    {
      Collections.sort(residences, new Comparator<Residence>() {
        @Override
        public int compare(Residence r1, Residence r2) {
          return r2.rent - r1.rent;
        }
      });
    }
    
    renderTemplate("AdminReports/index.html", residences);
  }
}
