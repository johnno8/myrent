package models;

import javax.persistence.Entity;
import play.db.jpa.Model;
import utils.LatLng;
import java.util.Date;
import javax.persistence.*;

@Entity
public class Residence extends Model
{
  public String geolocation;
  public String eircode;
  public int rent;
  public int numberBedrooms;
  public int numberBathrooms;
  public int area;
  public String residenceType;
  public Date registrationDate;
  
  //Residence's owner
  @ManyToOne
  public Landlord landlord;
  
  //Residence's tenant
  @OneToOne
  public Tenant tenant;
  
  /**
   * Returns a Google Map LatLng object representation of the residence's geolocation
   * @return
   */
  public LatLng getGeolocation()
  {
    return LatLng.toLatLng(geolocation);
  }
  
  /**
   * Returns the Residence object associated with the supplied eircode
   * @param eircode
   * @return
   */
  public static Residence findByEircode(String eircode)
  {
    return find("eircode", eircode).first();
  }
  
  /**
   * Adds a reference to the Residence's owner
   * @param landlord
   */
  public void addLandlord(Landlord landlord)
  {
    this.landlord = landlord;
    this.save();
  }
  
  /**
   * Adds a reference to the Residence's tenant
   * @param tenant
   */
  public void addTenant(Tenant tenant)
  {
    this.tenant = tenant;
    this.save();
  }
  
  /**
   * Removes the reference to a tenant ie. leaves the residence tenantless
   * @param tenant
   */
  public void removeTenant(Tenant tenant)
  {
    this.tenant = null;
    this.save();
  }
 
}
