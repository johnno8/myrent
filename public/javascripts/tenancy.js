  /**
  * Created by john on 16/08/2016.
  */
 //initialise the dropdown
 $('.ui.dropdown.newRental').dropdown();

 // validate the residence selection
 $('.ui.form.newRental').form({
   fields: {
     eircode: {
       identifier: 'eircode',
       rules: [{
         type: 'empty',
         prompt: 'Please select a vacant residence',
       },],
     },
   },
 });