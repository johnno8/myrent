package controllers;

import play.*;
import play.mvc.*;
import utils.Circle;
import utils.Geodistance;
import utils.LatLng;

import java.util.*;
import models.*;

public class Tenancy extends Controller
{
  /**
   * Populates a list of all tenants and another of all vacant residences
   * and renders them both in html page
   */
  public static void index()
  {
    Tenant tenant = Tenants.getCurrentTenant();
    
    List<Residence> residencesAll = Residence.findAll();
    List<Residence> vacantResidences = new ArrayList();
    for(Residence residence : residencesAll)
    {
      if(residence.tenant == null)
      {
    	  vacantResidences.add(residence);
      }
    }
    
	render(tenant, vacantResidences);
  }
  
  /**
   * Ends the logged in Tenant's current tenancy
   */
  public static void endTenancy()
  {
    terminateTenancy();
    index();
  }
  
  /**
   * Makes the logged in Tenant a tenant of the residence with the supplied
   * eircode
   * 
   * @param eircode	the eircode of the residence to be rented
   */
  public static void newRental(String eircode)
  {
	Tenant tenant = Tenants.getCurrentTenant();
    if(tenant.residence != null)
    {  
      Logger.info("Tenancy exists");
      terminateTenancy();
      Residence residence = Residence.findByEircode(eircode);
      residence.addTenant(tenant);
      tenant.save();
    }
    else
    {
      Residence residence = Residence.findByEircode(eircode);
      residence.addTenant(tenant);
      tenant.save();
    }
    index();
  }
  
  /**
   * Helper method to remove the reference to the logged in tenant from a residence
   * ie. end the tenancy
   */
  protected static void terminateTenancy()
  {
    Tenant tenant = Tenants.getCurrentTenant();
    
    if(tenant.residence != null)
    {
      Residence residence = Residence.findByEircode(tenant.residence.eircode);
      residence.removeTenant(tenant);
	  residence.save();
    }
  }
  
  /**
  *  Generates a Report instance relating to all residences within circle
  * @param radius    The radius (metres) of the search area
  * @param latcenter The latitude of the centre of the search area
  * @param lngcenter The longtitude of the centre of the search area
  */
  public static void tenantReport(double radius, double latcenter, double lngcenter)
  {
    // All reported residences will fall within this circle
    Circle circle = new Circle(latcenter, lngcenter, radius);
    Tenant tenant = Tenants.getCurrentTenant();
    List<Residence> residences = new ArrayList<Residence>();
    // Fetch all residences and filter out those within circle
    List<Residence> residencesAll = Residence.findAll();
    for (Residence res : residencesAll)
    {
      LatLng residenceLocation = res.getGeolocation();
      if (Geodistance.inCircle(residenceLocation, circle) && res.tenant == null)
      {
        residences.add(res);
      }
    }
    render("Tenancy/report.html", tenant, circle, residences);
  }
}
