 /**
  * Created by john on 16/08/2016.
  */
 //initialize the controls in the administration template
 $('.ui.dropdown.byRented').dropdown();
 $('.ui.dropdown.byType').dropdown();
 $('.ui.dropdown.bySortedRent').dropdown();

 //validate the byRented dropdown
 $('.ui.form.byRented').form({
   fields: {
     rentedStatus: {
       identifier: 'rentedStatus',
       rules: [{
         type: 'empty',
         prompt: 'Please select a rental status',
       },],
     },
   },

 });

 //validate the byType dropdown
 $('.ui.form.byType').form({
   fields: {
     residenceType: {
       identifier: 'residenceType',
       rules: [{
         type: 'empty',
         prompt: 'Please select a residence type',
       },],
     },
   },

 });

 //validate the bySortedRent dropdown
 $('.ui.form.bySortedRent').form({
   fields: {
     sortDirection: {
       identifier: 'sortDirection',
       rules: [{
         type: 'empty',
         prompt: 'Please select the direction to sort',
       },],
     },
   },

 });