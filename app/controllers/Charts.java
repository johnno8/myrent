package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
import org.json.simple.*;

public class Charts extends Controller
{
  public static void index()
  {
    List<Residence> residences = Residence.findAll();
    render(residences);
  }
  
  /**
   * Populates a json array with landlord and rent data
   * to be passed to piechart.js for rendering a pie chart
   */
  public static void listChartData()
  {
    List<Landlord> landlords = Landlord.findAll();
    List<List<String>> jsonArray = new ArrayList<List<String>>();
    
    int i = 0;
    int total = 0;
    
    for(Landlord landlord : landlords)
    {     
      for(Residence residence : landlord.residences)
      {
        total += residence.rent;  
      }     
    }
    
    for(Landlord landlord : landlords)
    {
      String llordInfo = landlord.firstName + " " + landlord.lastName;
      int rent = 0;
      
      for(Residence residence : landlord.residences)
      {
        rent += residence.rent;
      }
      float rentPercent = (float) (rent * 100) / total;
      
      jsonArray.add(i, Arrays.asList(llordInfo, String.valueOf(rentPercent)));
      i++;
    }
    renderJSON(jsonArray);
  }
}
