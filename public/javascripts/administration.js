/**
 * Created by john on 16/08/2016.
 */
// initialise the controls in the administration template
$('.ui.dropdown.deleteLandlord').dropdown();
$('.ui.dropdown.deleteTenant').dropdown();

// validate landlord selection
$('.ui.form.deleteLandlord').form({
  fields: {
    id: {
      identifier: 'id',
      rules: [{
        type: 'empty',
        prompt: 'Please select a landlord',
      },],
    },
  },

});

// validate tenant selection
$('.ui.form.deleteTenant').form({
  fields: {
    id: {
      identifier: 'id',
      rules: [{
        type: 'empty',
        prompt: 'Please select a tenant',
      },],
    },
  },

});