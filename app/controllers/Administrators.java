package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
import org.json.simple.*;

public class Administrators extends Controller
{
  public static void index()
  {
    render();
  }
  
  /**
   * Populates a list with all landlords and another with all tenants
   * and renders them in the html page
   */
  public static void administration()
  {
    List<Landlord> landlords = Landlord.findAll();
    List<Tenant> tenants = Tenant.findAll();
	render(landlords, tenants);  
  }
  
  /**
   * Login method for administrator, takes the user input email and password
   * as parameters and compares them with the hardcoded ones - if they match administrator
   * is logged in otherwise the login page is rerendered
   * @param email		User supplied email
   * @param password	User supplied password
   */
  public static void authenticate(String email, String password)
  {
    Administrator admin = new Administrator();
    admin.save();
	Logger.info("Attempting to authenticate with " + email + " : " + password);
	if(admin.checkEmail(email) && admin.checkPassword(password))
	{
      Logger.info("Authentication successful");
      session.put("logged_in_adminid", admin.id);
      session.put("logged_status", "logged_in");
      administration();
	}
	else
	{
      Logger.info("Authentication failed");
      index();
	}
  }
  
  /**
   * Logs out the current user
   */
  public static void logout()
  {
	setSessionLogout();
    Home.welcome();
  }
  
  /*
   * clear session on logout
   */
  protected static void setSessionLogout()
  {
    if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      session.remove("logged_in_adminid");
      session.put("logged_status", "logged_out");
      Logger.info("Session ended");
    }
  }
  
  /**
   * Removes a landlord, identified by the id parameter, from the database
   * @param id	The id of landlord to be deleted
   */
  public static void deleteLandlord(Long id)
  {
    Landlord landlord = Landlord.findById(id);
    
    landlord.delete();
    administration();
  }
  
  /**
   * Removes a tenant, identified by the id parameter, from the database
   * If the tenant still has a tenancy this nullified before deletion
   * @param id	The id of tenant to be deleted
   */
  public static void deleteTenant(Long id)
  {
    Tenant tenant = Tenant.findById(id);
    
    if(tenant.residence != null)
    {
      Residence residence = Residence.findByEircode(tenant.residence.eircode);
      residence.removeTenant(tenant);
	  residence.save();
    }
    
    tenant.delete();
    administration();
  }
  
  /**
   * Populates a json array with residence data to be passed to
   * adminmap.js in order to render map markers
   */
  public static void listMarkerData()
  {
    List<Residence> residences = Residence.findAll();
    List<List<String>> jsonArray = new ArrayList<List<String>>();
    int i = 0;
    for(Residence residence : residences)
    {
      String lat = residence.getGeolocation().latToString();
      String lng = residence.getGeolocation().lngToString();
      String residenceInfo;
      if(residence.tenant != null)
      {
        residenceInfo = "Eircode: " + residence.eircode + ", " + "Resident: " + residence.tenant.firstName + " " + residence.tenant.lastName;
      }
      else
      {
    	residenceInfo = "Eircode: " + residence.eircode + ", " + "Resident: Vacant";
      }
      
      jsonArray.add(i, Arrays.asList(residenceInfo, lat, lng));
      i++;
    }
    renderJSON(jsonArray);
  }
  
}
