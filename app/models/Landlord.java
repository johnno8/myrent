package models;

import javax.persistence.*;
import play.db.jpa.Model;
import java.util.*;

@Entity
public class Landlord extends Model
{
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  public String address1;
  public String address2;
  public String city;
  public String county;
  
  //List of all residences owned by a landlord
  @OneToMany(mappedBy="landlord",cascade=CascadeType.ALL)
  public List<Residence> residences = new ArrayList<Residence>();
  
  /**
   * Returns the landlord object associated with the supplied email
   * @param email
   * @return Landlord object
   */
  public static Landlord findByEmail(String email)
  {
    return find("email", email).first();
  }
  
  /**
   * Checks if supplied password matches saved one
   * @param password
   * @return true if passwords match
   */
  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }
  
  /**
   * Adds a new residence to the list of a lanlords residences
   * @param residence
   */
  public void addResidence(Residence residence)
  {
    residences.add(residence);
  }
}
