package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Landlords extends Controller
{
  /**
   * Renders Landlord signup page
   */
  public static void signup()
  {
	render();
  }
  
  /**
   * Renders Landlord signup page
   */
  public static void login()
  {
    render();
  }
  
  /**
   * Adds a new Landlord object to the database
   * @param landlord
   */
  public static void register(Landlord landlord)
  {
	landlord.save();
	
	Administrators.administration();
  }
  
  /**
   * Login method for Landlord, takes the user input email and password
   * as parameters, uses the supplied email to find the correct landlord and compares passwords
   *  - if they match landlord is logged in otherwise the login page is re-rendered
   * @param email		
   * @param password	
   */
  public static void authenticate(String email, String password)
  {
	Logger.info("Attempting to authenticate with " + email + " : " + password);
	Landlord landlord = Landlord.findByEmail(email);
    if((landlord != null) && (landlord.checkPassword(password) == true))
	{
	  Logger.info("Authentication successful");
	  session.put("logged_in_landlordid", landlord.id);
	  session.put("logged_status", "logged_in");
	  InputData.index();
	}
	else
	{
	  Logger.info("Authentication failed");
	  login();
	}
  }
  
  /**
   * Logs out the current user
   */
  public static void logout()
  {
	setSessionLogout();
    Home.welcome();
  }
  
  /*
   * clear session on logout
   */
  protected static void setSessionLogout()
  {
    if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      session.remove("logged_in_landlordid");
      session.put("logged_status", "logged_out");
      Logger.info("Session ended");
    }
  }
  
  /**
   * Returns the currently logged in landlord
   * @return
   */
  public static Landlord getCurrentLandlord()
	{
	  String userId = session.get("logged_in_landlordid");
	  if(userId == null)
	  {
	    return null;
	  }
	  Landlord logged_in_user = Landlord.findById(Long.parseLong(userId));
	  Logger.info("Logged in user is " + logged_in_user.firstName);
	  return logged_in_user;
	}
}
