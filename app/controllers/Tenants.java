package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Tenants extends Controller
{
  /**
   * Renders index page
   */
  public static void index()
  {
    render();
  }
  
  /**
   * Renders login page
   */
  public static void login()
  {
    render();
  }
  
  /**
   * Registers a tenant on the MyRent system
   * 
   * @param tenant	A tenant object with firstName, lastName, email and password fields
   */
  public static void register(Tenant tenant)
  {
    tenant.save();
    Logger.info("Tenant " + tenant.firstName + " sucessfully registered");
    Administrators.administration();
  }
  
  /**
   * Login method for Tenant, takes the user input email and password
   * as parameters, uses the supplied email to find the correct tenant and compares passwords
   *  - if they match tenant is logged in otherwise the login page is re-rendered
   * @param email		
   * @param password	
   */
  public static void authenticate(String email, String password)
  {
	Logger.info("Attempting to authenticate with " + email + " : " + password);
    Tenant tenant = Tenant.findByEmail(email);
    if((tenant != null) && (tenant.checkPassword(password) == true))
	{
	  Logger.info("Authentication successful");
	  session.put("logged_in_tenantid", tenant.id);
	  session.put("logged_status", "logged_in");
	  Tenancy.index();
	}
	else
	{
	  Logger.info("Authentication failed");
	  login();
	}
  }
  
  /**
   * Logs out the current user
   */
  public static void logout()
  {
	setSessionLogout();
    Home.welcome();
  }
  
  /*
   * clear session on logout
   */
  protected static void setSessionLogout()
  {
    if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      session.remove("logged_in_tenantid");
      session.put("logged_status", "logged_out");
      Logger.info("Session ended");
    }
  }
  
  /**
   * Returns the currently logged in tenant
   * @return
   */
  public static Tenant getCurrentTenant()
	{
	  String userId = session.get("logged_in_tenantid");
	  if(userId == null)
	  {
	    return null;
	  }
	  Tenant logged_in_user = Tenant.findById(Long.parseLong(userId));
	  Logger.info("Logged in Tenant is " + logged_in_user.firstName);
	  return logged_in_user;
	}
}
