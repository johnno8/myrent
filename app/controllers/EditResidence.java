package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import models.*;

public class EditResidence extends Controller
{
  /**
   * Renders index.html with the Residence object found using the supplied eircode
   * @param eircode	the eircode of the residence to be displayed
   */
  public static void index(String eircode)
  {
    Residence residence = Residence.findByEircode(eircode);
    render(residence);
  }
  
  /**
   * Changes the value of a residence's rent
   * @param id		the id of the Residence object to be edited
   * @param rent	the value that rent field is to be changed to
   */
  public static void updateResidence(Long id, int rent)
  {
    Residence residence = Residence.findById(id);
    Logger.info("Current rent: " + residence.rent);
    residence.rent = rent;
    Logger.info("Changed to: " + residence.rent);
    residence.save();
    LandlordConfig.index();
  }
}
